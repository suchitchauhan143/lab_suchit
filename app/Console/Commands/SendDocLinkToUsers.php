<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\Mail;
use Illuminate\Console\Command;
use App\issue_book;
use App\client;
use App\Jobs\ReturnSendEmail;
use App\Mail\remindermail;
use App\Notifications\notify;
use Illuminate\Contracts\Mail\Mailable;
use App\Mail\EmailReminder;
use Carbon\Carbon;
use DateTime;


class SendDocLinkToUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send email notification to user for reminder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $issue_data=issue_book::whereDate('return_date', '<=', Carbon::now())->where('status',1)->with(['clients'])->get();
        foreach($issue_data as $todo)
           {
               $email = $todo->clients->email;
               echo $email;
               Mail::to($email)->send(new remindermail($todo));
           }
    //   Mail::to( $data)->send(new remindermail($issue_book));
    }
}

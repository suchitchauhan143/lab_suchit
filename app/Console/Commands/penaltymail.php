<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\Mail;
use App\issue_book;
use App\Mail\penalty;
use App\client;
use Illuminate\Console\Command;
use Carbon\Carbon;

class penaltymail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'penaltysend:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
    $issue_data=issue_book::with(['clients'])->get();
    foreach($issue_data as $todo)
    {
     $email = $todo->clients->email;
     echo $email;
     Mail::to($email)->send(new penalty($todo));
    }
    }
}

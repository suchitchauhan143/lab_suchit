<?php

namespace App\Http\Controllers;

use App\add;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
Use Maatwebsite\Excel\Facades\Excel;

class AddController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $user=null;
    public function __construct(User $user)
    {
        $this->user = $user;
    }
    public function index()
    {
        //
    }
    public function add1(Request $request)
    {
        $data = request()->validate([
            'auther_name' => 'required|regex:/^[\pL\s\-]+$/u|max:255'
            ]);

        if($data)
        {
        $name = $request->input('auther_name');
        $data=array('name'=>$name);
        $d=add::insert($data);
        if($d)
        {
            return back()->with('success','Auther Added Successfully!');
        }
        else
        {
            return back()->with('error','Book Not Add Successfully!');
        }
        }
        else
        {
            return back()->with('warning','please check all given entries');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\add  $add
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $auther = add::paginate(10);
        return view('authershow')->with('auther',$auther);
    }

    public function delete(Request $request,$id)
    {
        $add=add::where('id',$id)->delete();
        if($add)
        {
            return back()->with('success','Auther Deleted Successfully!');
        }
        else
        {
            return back()->with('error','Auther Not Deleted Successfully!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\add  $add
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $auther = add::where('id',$id)->first();
        return view('autheredit')->with('auther',$auther);

    }
    public function edit1(Request $request,$id)
    {
       $update=add::where('id', $id)->update(['name'=>$request['auther_name']]);
        if($update)
        {
            return back()->with('success','Auther Updated Successfully!');
        }
        else
        {
            return back()->with('error','Auther Not Updated Successfully!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\add  $add
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, add $add)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\add  $add
     * @return \Illuminate\Http\Response
     */
    public function destroy(add $add)
    {
        //
    }
    public function export() 
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
    public function import() 
    {
        Excel::import(new UsersImport,request()->file('file'));
           
        return back();
    }
    public function importExportView()
    {
       return view('import');
    }
}

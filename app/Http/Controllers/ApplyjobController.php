<?php

namespace App\Http\Controllers;

use App\applyjob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApplyjobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$id)
    {
        $user_id = Auth::id();
        $data = request()->validate([
            'full_name' => 'required',
            'number' => 'required',
            'current_company' => 'required',
            'current_designation' => 'required',
            'current_location' => 'required',
            'current_salary' => 'required',
            'industry' => 'required',
            'functional_area' => 'required',
            'experience_year' => 'required',
            'experience_month' => 'required',
            'skill' => 'required',
            'reason' => 'required',
            ]);
            if($data)
            {
                $jobadd=new applyjob();
                $jobadd->full_name = $request->input('full_name');
                $jobadd->number = $request->input('number');
                $jobadd->current_company = $request->input('current_company');
                $jobadd->current_designation = $request->input('current_designation');
                $jobadd->current_location = $request->input('current_location');
                $jobadd->current_salary = $request->input('current_salary');
                $jobadd->industry = $request->input('industry');
                $jobadd->functional_area = $request->input('functional_area');
                $jobadd->experience_year = $request->input('experience_year');
                $jobadd->experience_month = $request->input('experience_month');
                $jobadd->skill = $request->input('skill');
                $jobadd->reason = $request->input('reason');
                $jobadd->user_id = $user_id;
                $jobadd->createjob_id = $id;

                $jobadd->save();
        if($jobadd)
        {
            return back()->with('success','Jobs Apply Successfully!');
        }
        else
        {
            return back()->with('error','Jobs Not Apply Successfully!');
        }
        }
        else
        {
            return back()->with('warning','please check all given entries');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\applyjob  $applyjob
     * @return \Illuminate\Http\Response
     */
    public function show(applyjob $applyjob)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\applyjob  $applyjob
     * @return \Illuminate\Http\Response
     */
    public function edit(applyjob $applyjob)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\applyjob  $applyjob
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, applyjob $applyjob)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\applyjob  $applyjob
     * @return \Illuminate\Http\Response
     */
    public function destroy(applyjob $applyjob)
    {
        //
    }
}

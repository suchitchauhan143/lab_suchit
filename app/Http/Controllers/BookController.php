<?php

namespace App\Http\Controllers;

use App\book;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\CustomClasses\ColectionPaginate;
use Illuminate\Support\Collection;
use Validator,Redirect,Response;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use PDF;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        $data = request()->validate([
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'price' => 'required|numeric',
            'auther_id' => 'required'
            ]);
        if($data)
        {
        $name = $request->input('name');
        $price = $request->input('price');
        $auther_id = $request->auther_id;
        $data=array('name'=>$name,"price"=>$price,'auth_id'=>$auther_id);
        $d=book::insert($data);
        if($d)
        {
            return back()->with('success','Book Added Successfully!');
        }
        else
        {
            return back()->with('error','Book Not Add Successfully!');
        }
        }
        else
        {
            return back()->with('warning','please check all given entries');
        }
    }
    public function show(Request $request)
    {
        $book = book::with(['author'])->paginate(10);
        return view('show',compact('book'));
    }

    public function delete(Request $request,$id)
    {
        $book=book::where('id',$id)->delete();
        if($book)
        {
            return back()->with('success','successfully deleted');
        }
        else
        {
            return back()->with('error','Book Not Delete Successfully!');
        }
    }
    public function pdf(Request $request)
    {
        $book=book::with(['author'])->get();
        $image_url = public_path().'/image'.'/'.'book.jpg';
        $image = file_get_contents($image_url);
        if ($image !== false){
            $base64 = 'data:image/jpg;base64,'.base64_encode($image);
        }
        $data = [
            'book' => $book,
            'image' => $base64
        ];
        $pdf = PDF::loadView('bookpdf',['data' => $data]);
        $pdf->setPaper('L', 'landscape');
        $pdf->setOptions(['isPhpEnabled' => true]);
        $return= $pdf->download('bookdata.pdf');
        return $return;
    }
    public function search(Request $request)
    {
        $search = $request->input('search');
        $book1= book::query()
        ->where('name', 'LIKE', "%{$search}%")
        ->get();
        return view('show1',compact('book1'));
    }
    public function edit($id)
    {
        $users = book::where('id',$id)->first();
        return view('bookedit')->with('users',$users);
    }
    public function edit1(Request $request,$id)
    {
        $update=book::where('id', $id)->update(['name'=>$request['name'],'price'=>$request['price']]);
        if($update)
        {
            return back()->with('success','Book Updated Successfully!');
        }
        else
        {
            return back()->with('error','Not Updated Successfully!');
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, book $book)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(book $book)
    {
        //
    }
}

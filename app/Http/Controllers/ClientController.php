<?php

namespace App\Http\Controllers;

use App\client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function add(Request $request)
    {

        $data = request()->validate([
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'date' => 'required|date',
            'address' => 'required|regex:/([- ,\/0-9a-zA-Z]+)/',
            'phone' => 'required|min:10|numeric',
            'deposit' => 'required|numeric',
            'email' => 'required|email'
            ]);
            if($data)
            {
        $name = $request->input('name');
        $date = $request->input('date');
        $address = $request->input('address');
        $phone = $request->input('phone');
        $deposit = $request->input('deposit');
        $email = $request->input('email');
        $data=array('name'=>$name,"date_of_birth"=>$date,"address"=>$address,"phone_number"=>$phone,
        "deposit"=>$deposit,"email"=>$email);
        $d=client::insert($data);
        if($d)
        {
            return back()->with('success','Client Added Successfully!');
        }
        else
        {
            return back()->with('error','Client Not Add Successfully!');
        }
        }
        else
        {
            return back()->with('warning','please check all given entries');
        }
    }
    public function show()
    {
        $client = client::paginate(10);
        return view('clientshow')->with('client',$client);
    }

    public function delete($id)
    {
        $client1=client::where('id',$id)->delete();
        if($client1)
        {
            return redirect()->route('client/show')->with('success','successfully deleted');
        }
        else
        {
            return back()->with('error','Client Not delete Successfully!');
        }
    }
    public function edit($id)
    {
        $client = client::where('id',$id)->first();
        return view('clientedit')->with('client',$client);

    }
    public function edit1(Request $request,$id)
    {
        $update=client::where('id', $id)->update(['name'=>$request['name'],'date_of_birth'=>$request['date'],
        'address'=>$request['address'],'phone_number'=>$request['phone'],'deposit'=>$request['deposit'],
        'email'=>$request['email']]);
        if($update)
        {
            return back()->with('success','Client Updated Successfully!');
        }
        else
        {
            return back()->with('error','Client Not Updated Successfully!');
        }

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\client  $client
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\client  $client
     * @return \Illuminate\Http\Response
     */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, client $client)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(client $client)
    {
    }
}

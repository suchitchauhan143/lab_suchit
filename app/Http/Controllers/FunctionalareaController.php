<?php

namespace App\Http\Controllers;

use App\functionalarea;
use Illuminate\Http\Request;

class FunctionalareaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function show()
    {
        $functional = functionalarea::get();
        return view('functional_show')->with('functional',$functional);
    }
    
    public function delete($id)
    {
        $functionalD=functionalarea::where('id',$id)->delete();
        
        if($functionalD)
        {
            return redirect()->route('functional/show');
        }
        else    
        {
            dd("Not successfully");
        }
    }
    public function edit($id)
    {
        
        $functionalE = functionalarea::where('id',$id)->first();
        return view('functional_edit')->with('functionalE',$functionalE);
        
    }
    public function edit1(Request $request,$id)
    {
        
       $update=functionalarea::where('id', $id)->update(['name'=>$request['name']]);
        if($update)
        {
            return back()->withFlashSuccess("updated");
        }
        else
        {
            dd("Not successfully updated");
        }
        
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\functionalarea  $functionalarea
     * @return \Illuminate\Http\Response
     */
    // public function show(functionalarea $functionalarea)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\functionalarea  $functionalarea
     * @return \Illuminate\Http\Response
     */
    // public function edit(functionalarea $functionalarea)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\functionalarea  $functionalarea
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, functionalarea $functionalarea)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\functionalarea  $functionalarea
     * @return \Illuminate\Http\Response
     */
    public function destroy(functionalarea $functionalarea)
    {
        //
    }

    public function add(Request $request)
    {
        
        $data = request()->validate([
            'name' => 'required',
            
            ]);
            
        if($data)
        {
        $name = $request->input('name');
        
        $data=array('name'=>$name);
        $d=functionalarea::insert($data);
        if($d)
        {
            // $googleAPIToken = env('GOOGLE_API_TOKEN');
            // dd($googleAPIToken);
            return back()->withFlashSuccess("inserted");
        }
        else
        {
            dd("Not successfully add");
        }
        }
        else
        {
            dd("Not successfully validated");
        }
    }

    
}

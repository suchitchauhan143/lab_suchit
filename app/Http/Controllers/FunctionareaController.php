<?php

namespace App\Http\Controllers;

use App\functionarea;
use Illuminate\Http\Request;

class FunctionareaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add1(Request $request)
    {
        $data = request()->validate([
            'function_name' => 'required|regex:/^[\pL\s\-]+$/u|max:255'
            ]);

        if($data)
        {
        $name = $request->input('function_name');
        $data=array('name'=>$name);
        $d=functionarea::insert($data);
        if($d)
        {
            return back()->with('success','Functionarea Added Successfully!');
        }
        else
        {
            return back()->with('error','Functionarea Not Add Successfully!');
        }
        }
        else
        {
            return back()->with('warning','please check all given entries');
        }
    }

    public function show()
    {
        $function = functionarea::paginate(10);
        return view('functionshow')->with('function',$function);
    }

    public function delete(Request $request,$id)
    {
        $delete=functionarea::where('id',$id)->delete();
        if($delete)
        {
            return back()->with('success','Functionarea Deleted Successfully!');
        }
        else
        {
            return back()->with('error','Functionarea Not Deleted Successfully!');
        }
    }

    public function edit($id)
    {
        $function = functionarea::where('id',$id)->first();
        return view('functionedit')->with('function',$function);

    }
    public function edit1(Request $request,$id)
    {
       $update=functionarea::where('id', $id)->update(['name'=>$request['function_name']]);
        if($update)
        {
            return back()->with('success','functionarea Updated Successfully!');
        }
        else
        {
            return back()->with('error','functionarea Not Updated Successfully!');
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\functionarea  $functionarea
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\functionarea  $functionarea
     * @return \Illuminate\Http\Response
     */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\functionarea  $functionarea
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, functionarea $functionarea)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\functionarea  $functionarea
     * @return \Illuminate\Http\Response
     */
    public function destroy(functionarea $functionarea)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Jobs\pdfmaker;
use App\jobs\TestSendEmail;
use App\jobs\ReturnSendEmail;
use App\jobs\reissue;
use App\issue_book;
use App\book;
use App\client;
use Illuminate\Support\Facades\Auth;
use App\Console\Commands\SendDocLinkToUsers;
use Carbon\Carbon;
use PDF;

class IssueBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function delete($id)
    {
        $issue_book=issue_book::where('id',$id)->delete();
        if($issue_book)
        {
            return redirect()->route('issue')->with('success','successfully deleted');
        }
        else
        {
            return back()->with('error','Issued Book Not Deleted Successfully!');
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\issue_book  $issue_book
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $issue = issue_book::with(['book_details'],['clients'])->paginate(10);
        return view('issue')->with('issue',$issue);
    }

    public function add(Request $request)
    {
        $data = request()->validate([
            'date' => 'required|date',
            'date1' => 'required|date',
            'book_id' => 'required',
            'client_id' => 'required'
            ]);
        if($data)
        {
        $issue_book=new issue_book();
        $issue_book->issue_date = $request->input('date');
        $issue_book->return_date = $request->input('date1');
        $issue_book->book_id = $request->book_id;
        $issue_book->client_id = $request->client_id;
        $startTime = Carbon::parse( $issue_book->issue_date);
        $endTime = Carbon::parse( $issue_book->return_date);
        $totalDuration = $endTime->diffInDays($startTime);
        $issue_book->days=$totalDuration;
        $issue_book->save();
        if($issue_book)
        {
            $issue_book=issue_book::with(['clients'])->find($issue_book->id);
            $this->dispatch(new TestSendEmail($issue_book));
            return back()->with('success','Book Issued Successfully!');
        }
        else
        {
            return back()->with('error','Book Not Issued Successfully!');
        }
        }
        else
        {
            return back()->with('warning','please check all given entries');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\issue_book  $issue_book
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $issue = issue_book::where('id',$id)->first();
        return view('issuebookedit')->with('issue',$issue);
    }

    public function edit1(Request $request,$id)
    {
       $update=issue_book::where('id', $id)->update(['issue_date'=>$request['date'],'return_date'=>$request['date1']]);
        if($update)
        {
            return back()->with('success','Issued Book Updated Successfully!');
        }
        else
        {
            return back()->with('error','Issued Book Not Updated Successfully!');
        }
    }
    public function status(Request $request)

    {
        $user = issue_book::with(['clients'])->find($request->id);
        $user->status = $request->status;
        $user->save();
        if($request->get('status')==1)
        {
        $this->dispatch(new ReturnSendEmail($user));
        return response()->json(['success'=>'Status change successfully.']);
        }
        else
        {
            $this->dispatch(new reissue($user));
            return response()->json(['success'=>'Status change successfully.']);
        }
    }
    public function pdf(Request $request,$id)
    {
        $book = issue_book::where('id',$id)->with(['clients','book_details'])->get();
        $user = issue_book::with(['clients'])->find($request->id);
        $pdf = PDF::loadView('pdf', compact('book'));
        $return= $pdf->download('issue_bookdata.pdf');
        $this->dispatch(new pdfmaker($user,$pdf));
        return $return;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\issue_book  $issue_book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, issue_book $issue_book)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\issue_book  $issue_book
     * @return \Illuminate\Http\Response
     */
    public function destroy(issue_book $issue_book)
    {
        //
    }
}

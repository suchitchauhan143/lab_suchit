<?php

namespace App\Http\Controllers;

use App\jobadd;
use App\Http\Controllers\Functionalarea;
use App\Http\Controllers\jobmake;
use Illuminate\Http\Request;


class JobaddController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\jobadd  $jobadd
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $jobs = jobadd::with(['functionalarea'],['jobcategories'])->get();
        return view('joaaddsee_show')->with('jobs',$jobs);
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\jobadd  $jobadd
     * @return \Illuminate\Http\Response
     */
    public function edit(jobadd $jobadd)
    {
        //11``  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\jobadd  $jobadd
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, jobadd $jobadd)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\jobadd  $jobadd
     * @return \Illuminate\Http\Response
     */
    public function destroy(jobadd $jobadd)
    {
        //
    }
    
    public function add(Request $request)
    {
        
        $data = request()->validate([
            'job_title' => 'required',
            'job_description' => 'required',
            'location' => 'required',
            'job_time' => 'required',
        
            'work_from_home' => 'required',
            'vacancies' => 'required',
            'gender' => 'required',
            'minimum_age' => 'required',
            'maximum_age' => 'required',
            'qualification' => 'required',
            'experience' => 'required',
            
            'benefits' => 'required',
            'currency'=>'required',
            'minimum_salary' => 'required',
            'maximum_salary' => 'required',
            'organization_name' => 'required',
            
            'about_organization' => 'required',
            'contact' => 'required',
            'skill' => 'required'
            
         ]);
        if($data)
        {
        $jobadd=new jobadd();
        $jobadd->job_title = $request->input('job_title');
        $jobadd->job_description = $request->input('job_description');

        $jobadd->location = $request->input('location');
        
        $jobadd->functional_id = $request->input('functional_area');
        $jobadd->category_id = $request->input('job_category');
        
        $jobadd->job_time = $request->input('job_time');


        $jobadd->work_from_home = $request->input('work_from_home');
        $jobadd->vacancies = $request->input('vacancies');
        $jobadd->gender = $request->input('gender');


        $jobadd->minimum_age = $request->input('minimum_age');
        $jobadd->maximum_age = $request->input('maximum_age');
        $jobadd->qualification = $request->input('qualification');
        $jobadd->experience = $request->input('experience');
        
        $jobadd->benefits = $request->input('benefits');
        
        $jobadd->currency = $request->input('currency');
        $jobadd->minimum_salary = $request->input('minimum_salary');
        $jobadd->maximum_salary = $request->input('maximum_salary');
        $jobadd->organization_name = $request->input('organization_name');

        $jobadd->about_organization = $request->input('about_organization');
        $jobadd->contact = $request->input('contact');
        $jobadd->skill=$request->input('skill');

    
        $jobadd->save();

        if($jobadd)
        {
           
             return back()->withFlashSuccess("inserted");
        }
        else
        {
            dd("Not successfully add");
        }
        }
        else
        {
            dd("Not successfully validated");
        }
    }
}

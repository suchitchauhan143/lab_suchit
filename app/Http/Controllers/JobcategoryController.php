<?php

namespace App\Http\Controllers;

use App\jobcategory;
use App\functionarea;
use Illuminate\Http\Request;

class JobcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function add1(Request $request)
    {
        $data = request()->validate([
            'job_name' => 'required|regex:/^[\pL\s\-]+$/u|max:255'
            ]);

        if($data)
        {
        $name = $request->input('job_name');
        $data=array('name'=>$name);
        $d=jobcategory::insert($data);
        if($d)
        {
            return back()->with('success','jobcategory Added Successfully!');
        }
        else
        {
            return back()->with('error','jobcategory Not Add Successfully!');
        }
        }
        else
        {
            return back()->with('warning','please check all given entries');
        }
    }

    public function show()
    {
        $job = jobcategory::paginate(10);
        return view('jobcategoryshow')->with('job',$job);
    }

    public function delete(Request $request,$id)
    {
        $delete=jobcategory::where('id',$id)->delete();
        if($delete)
        {
            return back()->with('success','Functionarea Deleted Successfully!');
        }
        else
        {
            return back()->with('error','Functionarea Not Deleted Successfully!');
        }
    }

    public function edit($id)
    {
        $job = jobcategory::where('id',$id)->first();
        return view('jobcategoryedit')->with('job',$job);

    }
    public function edit1(Request $request,$id)
    {
       $update=jobcategory::where('id', $id)->update(['name'=>$request['job_name']]);
        if($update)
        {
            return back()->with('success','jobcategory Updated Successfully!');
        }
        else
        {
            return back()->with('error','jobcategory Not Updated Successfully!');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\jobcategory  $jobcategory
     * @return \Illuminate\Http\Response
     */
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\jobcategory  $jobcategory
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\jobcategory  $jobcategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, jobcategory $jobcategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\jobcategory  $jobcategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(jobcategory $jobcategory)
    {
        //
    }
}

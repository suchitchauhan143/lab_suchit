<?php

namespace App\Http\Controllers;

use App\jobmake;
use Illuminate\Http\Request;

class JobmakeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function show()
    {
        $jobs = jobmake::get();
        return view('job_Show')->with('jobs',$jobs);
    }

    public function edit($id)
    {
        
        $JobsE = jobmake::where('id',$id)->first();
        return view('jobedit')->with('JobsE',$JobsE);
        
    }

    public function edit1(Request $request,$id)
    {
        
       $update=jobmake::where('id', $id)->update(['name'=>$request['name']]);
        if($update)
        {
            return back()->withFlashSuccess("updated");
        }
        else
        {
            dd("Not successfully updated");
        }
        
    }

    public function delete($id)
    {
        $functionalD=jobmake::where('id',$id)->delete();
        
        if($functionalD)
        {
            return redirect()->route('Jobs/show');
        }
        else    
        {
            dd("Not successfully");
        }
    }

    public function add(Request $request)
    {
        
        $data = request()->validate([
            'name' => 'required',
            
            ]);
            
        if($data)
        {
        $name = $request->input('name');
        
        $data=array('name'=>$name);
        $d=jobmake::insert($data);
        if($d)
        {
            // $googleAPIToken = env('GOOGLE_API_TOKEN');
            // dd($googleAPIToken);
            return back()->withFlashSuccess("inserted");
        }
        else
        {
            dd("Not successfully add");
        }
        }
        else
        {
            dd("Not successfully validated");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\jobmake  $jobmake
     * @return \Illuminate\Http\Response
     */
    // public function show(jobmake $jobmake)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\jobmake  $jobmake
     * @return \Illuminate\Http\Response
     */
    // public function edit(jobmake $jobmake)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\jobmake  $jobmake
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, jobmake $jobmake)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\jobmake  $jobmake
     * @return \Illuminate\Http\Response
     */
    public function destroy(jobmake $jobmake)
    {
        //
    }
}

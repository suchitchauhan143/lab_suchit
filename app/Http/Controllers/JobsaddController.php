<?php

namespace App\Http\Controllers;

use App\applyjob;
use App\jobsadd;
use App\jobcategory;
use App\functionarea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class JobsaddController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add1(Request $request)
    {
        $id=Auth::id();
        $data = request()->validate([
            'job_title' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'job_description' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'location' => 'required',
            'functional_area' => 'required',
            'job_category' => 'required',
            'job_time' => 'required',
            'work_from_home' => 'required',
            'vacancies' => 'required',
            'gender' => 'required',
            'minimum_age' => 'required',
            'maximum_age' => 'required',
            'qualification' => 'required',
            'experience' => 'required',
            'benefits' => 'required',
            'currency' => 'required',
            'minimum_salary' => 'required',
            'maximum_salary' => 'required',
            'organization_name' => 'required',
            'about_organization' => 'required',
            'contact' => 'required',
            'skill' => 'required',
            ]);
            if($data)
            {
                $jobadd=new jobsadd();
                $jobadd->job_title = $request->input('job_title');
                $jobadd->job_description = $request->input('job_description');

                $jobadd->location = $request->input('location');

                $jobadd->functional_id = $request->input('functional_area');
                $jobadd->jobcategory_id = $request->input('job_category');

                $jobadd->job_time = $request->input('job_time');


                $jobadd->work_from_home = $request->input('work_from_home');
                $jobadd->vacancies = $request->input('vacancies');
                $jobadd->gender = $request->input('gender');


                $jobadd->minimum_age = $request->input('minimum_age');
                $jobadd->maximum_age = $request->input('maximum_age');
                $jobadd->qualification = $request->input('qualification');
                $jobadd->experience = $request->input('experience');

                $jobadd->benefits = $request->input('benefits');

                $jobadd->currency = $request->input('currency');
                $jobadd->minimum_salary = $request->input('minimum_salary');
                $jobadd->maximum_salary = $request->input('maximum_salary');
                $jobadd->organization_name = $request->input('organization_name');

                $jobadd->about_organization = $request->input('about_organization');
                $jobadd->contact = $request->input('contact');
                $jobadd->skill=$request->input('skill');

                $jobadd->user_id=$id;
                $jobadd->save();
        if($jobadd)
        {
            return back()->with('success','Jobs Added Successfully!');
        }
        else
        {
            return back()->with('error','Jobs Not Add Successfully!');
        }
        }
        else
        {
            return back()->with('warning','please check all given entries');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\jobsadd  $jobsadd
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $data= jobsadd::get();
        return view('jobshow')->with('data',$data);
    }
    public function delete(Request $request,$id)
    {
        $delete=jobsadd::where('id',$id)->delete();
        if($delete)
        {
            return back()->with('success','Jobs Deleted Successfully!');
        }
        else
        {
            return back()->with('error','Jobs Not Deleted Successfully!');
        }
    }

    public function edit($id)
    {
        $function=functionarea::get();
       $jobcategory=jobcategory::get();
        $job = jobsadd::where('id',$id)->first();
        return view('jobsedit')->with('job',$job)->with('function',$function)->with('jobcategory',$jobcategory);

    }
    public function edit1(Request $request,$id)
    {
        $update= jobsadd::where('id', $id)
        ->update(['job_title' => $request->input('job_title'),
                 'job_description'=>$request->input('job_description'),
                 'location'=>$request->input('location'),
                 'functional_id'=>$request->input('functional_area'),
                 'jobcategory_id'=>$request->input('job_category'),
                 'job_time'=>$request->input('job_time'),
                 'work_from_home'=>$request->input('work_from_home'),
                 'vacancies'=>$request->input('vacancies'),
                 'gender'=>$request->input('gender'),
                 'minimum_age'=>$request->input('minimum_age'),
                 'maximum_age'=>$request->input('maximum_age'),
                 'qualification'=>$request->input('qualification'),
                 'experience'=>$request->input('experience'),
                 'benefits'=>$request->input('benefits'),
                 'currency'=>$request->input('currency'),
                 'minimum_salary'=>$request->input('minimum_salary'),
                 'maximum_salary'=>$request->input('maximum_salary'),
                 'organization_name'=>$request->input('organization_name'),
                 'about_organization'=>$request->input('about_organization'),
                 'contact'=>$request->input('contact'),
                 'skill'=>$request->input('skill'),
                 ]
                );
        if($update)
        {
            return back()->with('success','jobcategory Updated Successfully!');
        }
        else
        {
            return back()->with('error','jobcategory Not Updated Successfully!');
        }
    }
    public function apply($id)
    {
        return view('applyjob')->with('id',$id);
    }
    public function index(Request $request)
    {
      if ($request->ajax()) {
        $data = jobsadd::latest()->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($data){

                       $btn = '<a href="/deletejob/'. $data->id .'" class="edit btn btn-primary btn-sm">Delete</a>';
                       $btn.= '<a href="/updatejob/'. $data->id .'" class="edit btn btn-primary btn-sm">Update</a>';
                        return $btn;
                })
                ->addColumn('Apply_Job', function($data){

                    $btn = '<a href="/applyjob/'. $data->id .'" class="edit btn btn-primary btn-sm">Apply Job</a>';
                     return $btn;
                })
                ->addColumn('Paticipantes', function($data){

                $btn = '<a href="/participants/'. $data->id .'" class="edit btn btn-primary btn-sm">participants</a>';
                 return $btn;
                })
                ->rawColumns(['action','Apply_Job','Paticipantes'])
                ->make(true);
    }
    return view('jobshow');
}

    // }
    public function participant($id)
    {
        $participant=applyjob::where('createjob_id',$id)->get();
        return view('participant')->with('participant',$participant);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\jobsadd  $jobsadd
     * @return \Illuminate\Http\Response
     */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\jobsadd  $jobsadd
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, jobsadd $jobsadd)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\jobsadd  $jobsadd
     * @return \Illuminate\Http\Response
     */
    public function destroy(jobsadd $jobsadd)
    {
        //
    }
}

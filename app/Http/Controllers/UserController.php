<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use App\User;
use App\book;
use App\add;
use App\issue_book;
use App\client;
use App\functionarea;
use App\jobcategory;
use Session;
class UserController extends Controller
{
   public function LogIn(Request $request)
   {
    $user = $request->name;
    $pass  = $request->password;
    $user1 = User::where('email', $user)->first();
    if ($user1) {

        $user2=User::where('password',$pass)->first();
        if($user2)
        {

           return view('home');
        }
        else
        {

            die("456");
        }
    }
    else {
        return view('auth.register');
    }
    }
    public function register()
    {
        return view('auth.register');
    }
    public function register1(Request $request)
    {
        $user=new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->save();
        if($user)
        {
             return view('auth.login');
        }
        else
        {
            dd("Not successfully add");
        }
    }
   public function index()
   {
    $auther_details=add::get();
    return view('home')->with('auther_details',$auther_details);
   }
   public function index1()
   {

       return view('auther');
    }
   public function index2()
   {
    $issue = book::get();
    $issue1=client::get();
    return view('issueadd')->with('issue',$issue)->with('issue1',$issue1);
   }
   public function index3()
   {
       return view('client');
    }
   public function index4()
   {
       return view('booktype');
   }
   public function index7()
   {
       return view('functionarea');
   }
   public function index8()
   {
       return view('jobcategory');
   }
   public function index9()
   {
       $function=functionarea::get();
       $jobcategory=jobcategory::get();
       return view('jobs')->with('function',$function)->with('jobcategory',$jobcategory);
   }
   public function index5(Request $request)
   {
        Auth::logout();
    $request->session()->flush();
    $request->session()->regenerate();
       return view('auth.login');
   }
   public function index6()
   {
       return view('auth.login');
   }
}

<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\applyjob;

class applyjobapi extends Controller
{
    public function index()
    {
        $function=applyjob::get();
        return $function;
    }

    public function store(Request $request)
    {

        $jobadd= new applyjob;
        $jobadd->full_name = $request->input('full_name');
        $jobadd->number = $request->input('number');
        $jobadd->current_company = $request->input('current_company');
        $jobadd->current_designation = $request->input('current_designation');
        $jobadd->current_location = $request->input('current_location');
        $jobadd->current_salary = $request->input('current_salary');
        $jobadd->industry = $request->input('industry');
        $jobadd->functional_area = $request->input('functional_area');
        $jobadd->experience_year = $request->input('experience_year');
        $jobadd->experience_month = $request->input('experience_month');
        $jobadd->skill = $request->input('skill');
        $jobadd->reason = $request->input('reason');
        $jobadd->user_id = 1;
        $jobadd->createjob_id = $request->input('createjob');
        $jobadd->save();
        return $jobadd;
    }

    public function destroy($id)
    {
        $task = applyjob::findorFail($id); //searching for object in database using ID
      if($task->delete()){ //deletes the object
          return 'deleted successfully'; //shows a message when the delete operation was successful.
      }
    }


    public function update(Request $request,$id){

      $task = applyjob::findorFail($id); // uses the id to search values that need to be updated.
      
      $task->name=$request->input('name');
      $task->address=$request->input('address');
      $task->date_of_birth=$request->input('date_of_birth');
      $task->email=$request->input('email');
      $task->deposit=$request->input('deposit');
      $task->phone_number=$request->input('phone_number'); 
      
      $task->save();//saves the values in the database. The existing data is overwritten.
      return $task;
    }

}

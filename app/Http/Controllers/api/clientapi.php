<?php

namespace App\Http\Controllers\api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\client;

class clientapi extends Controller
{
    public function index()
    {
        $function=client::get();
        return $function;
    }

    public function store(Request $request){

        $function= new client;
        $function->name=$request->input('name');
        $function->address=$request->input('address');
        $function->date_of_birth=$request->input('date_of_birth');
        $function->email=$request->input('email');
        $function->deposit=$request->input('deposit');
        $function->phone_number=$request->input('phone_number');
        
        $function->save();
        return $function;
    }

    public function destroy($id)
    {
        $task = client::findorFail($id); //searching for object in database using ID
      if($task->delete()){ //deletes the object
          return 'deleted successfully'; //shows a message when the delete operation was successful.
      }
    }


    public function update(Request $request,$id){

      $task = client::findorFail($id); // uses the id to search values that need to be updated.
      
      $task->name=$request->input('name');
      $task->address=$request->input('address');
      $task->date_of_birth=$request->input('date_of_birth');
      $task->email=$request->input('email');
      $task->deposit=$request->input('deposit');
      $task->phone_number=$request->input('phone_number'); 
      
      $task->save();//saves the values in the database. The existing data is overwritten.
      return $task;
    }

}

<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\jobsadd;

class jobapply extends Controller
{
    public function index()
    {
        $function=jobsadd::get();
        return $function;
    }

    public function store(Request $request){

        $jobadd= new jobsadd;
        $jobadd->job_title = $request->input('job_title');
        $jobadd->job_description = $request->input('job_description');
        $jobadd->location = $request->input('location');
        $jobadd->functional_id = $request->input('functional_area');
        $jobadd->jobcategory_id = $request->input('job_category');
        $jobadd->job_time = $request->input('job_time');
        $jobadd->work_from_home = $request->input('work_from_home');
        $jobadd->vacancies = $request->input('vacancies');
        $jobadd->gender = $request->input('gender');
        $jobadd->minimum_age = $request->input('minimum_age');
        $jobadd->maximum_age = $request->input('maximum_age');
        $jobadd->qualification = $request->input('qualification');
        $jobadd->experience = $request->input('experience');
        $jobadd->benefits = $request->input('benefits');
        $jobadd->currency = $request->input('currency');
        $jobadd->minimum_salary = $request->input('minimum_salary');
        $jobadd->maximum_salary = $request->input('maximum_salary');
        $jobadd->organization_name = $request->input('organization_name');
        $jobadd->about_organization = $request->input('about_organization');
        $jobadd->contact = $request->input('contact');
        $jobadd->skill=$request->input('skill');
        $jobadd->user_id=1;
        $jobadd->save();
        return $jobadd;
    }

    public function destroy($id)
    {
        $task = jobsadd::findorFail($id); //searching for object in database using ID
      if($task->delete()){ //deletes the object
          return 'deleted successfully'; //shows a message when the delete operation was successful.
      }
    }


    public function update(Request $request,$id)
    {
      $jobadd = jobsadd::findorFail($id); // uses the id to search values that need to be updated.
      $jobadd->job_title = $request->input('job_title');
      $jobadd->job_description = $request->input('job_description');
      $jobadd->location = $request->input('location');
      $jobadd->functional_id = $request->input('functional_area');
      $jobadd->jobcategory_id = $request->input('job_category');
      $jobadd->job_time = $request->input('job_time');
      $jobadd->work_from_home = $request->input('work_from_home');
      $jobadd->vacancies = $request->input('vacancies');
      $jobadd->gender = $request->input('gender');
      $jobadd->minimum_age = $request->input('minimum_age');
      $jobadd->maximum_age = $request->input('maximum_age');
      $jobadd->qualification = $request->input('qualification');
      $jobadd->experience = $request->input('experience');
      $jobadd->benefits = $request->input('benefits');
      $jobadd->currency = $request->input('currency');
      $jobadd->minimum_salary = $request->input('minimum_salary');
      $jobadd->maximum_salary = $request->input('maximum_salary');
      $jobadd->organization_name = $request->input('organization_name');
      $jobadd->about_organization = $request->input('about_organization');
      $jobadd->contact = $request->input('contact');
      $jobadd->skill=$request->input('skill');
      $jobadd->user_id=1;
      $jobadd->save();
      return $jobadd;
    }}

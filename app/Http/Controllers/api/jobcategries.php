<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\jobcategory;

class jobcategries extends Controller
{
    public function index()
    {
        $function=jobcategory::get();
        return $function;
    }

    public function store(Request $request){

        $function= new jobcategory;
        $function->name=$request->input('name');
        $function->save();
        return $function;
    }

    public function destroy($id)
    {
        $task = jobcategory::findorFail($id); //searching for object in database using ID
      if($task->delete()){ //deletes the object
          return 'deleted successfully'; //shows a message when the delete operation was successful.
      }
    }


    public function update(Request $request,$id){

      $task = jobcategory::findorFail($id); // uses the id to search values that need to be updated.
      $task->name = $request->input('name'); //retrieves user input
      $task->save();//saves the values in the database. The existing data is overwritten.
      return $task;
    }

}

<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class users extends Controller
{
    public function index()
    {
        $function=User::get();
        return $function;
    }

    public function store(Request $request){

        $function= new User;
        $function->name=$request->input('name');
        $function->email=$request->input('email');
        $function->save();
        return $function;
    }

    public function destroy($id)
    {
        $task = User::findorFail($id); //searching for object in database using ID
      if($task->delete()){ //deletes the object
          return 'deleted successfully'; //shows a message when the delete operation was successful.
      }
    }


    public function update(Request $request,$id){

      $task = User::findorFail($id); // uses the id to search values that need to be updated.
      $task->name = $request->input('name'); //retrieves user input
      $task->save();//saves the values in the database. The existing data is overwritten.
      return $task;
    }

}

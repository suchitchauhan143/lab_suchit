<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class applyjob extends Model
{
    public $table = "applyjobs";

    protected $primaryKey = 'id';
    protected $fillable = [
        'full_name', 'user_id', 'createjob_id', 'number', 'current_company', 'current_designation', 'current_location', 'current_salary', 'industry'. 'functional_area', 'experience_year', 'experience_month', 'skill', 'reason'
    ];

    public function users(){
        return $this->hasOne('App\User','id','user_id');
    }
    // public function clients(){
    //     return $this->hasMany(client::class);
    // }

    public function jobcategory(){
        return $this->hasOne('App\jobcategory','id','createjob_id');
    }
}

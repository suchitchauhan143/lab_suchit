<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class book extends Model
{
    public $table = "book_details";
    protected $primaryKey = 'id';
    protected $fillable=[
        'name','price'
    ];

    public function author(){
        return $this->hasOne('App\add','id','auth_id');
    }

    public function issue_books(){
        return $this->hasMany(issue_book::class);
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class client extends Model
{
    public $table = "clients";
    protected $primaryKey = 'id';
    protected $fillable=[
        'name','address','date_of_birth','email','deposit','phone_number'
    ];
    public function issue_books(){
        return $this->hasMany(issue_book::class);
    }
}

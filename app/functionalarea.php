<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class functionalarea extends Model
{
    public $table = "functionalareas";
    protected $fillable=[
        'name',
    ];

    public function functionalarea(){
        return $this->hasmany(functionalarea::class);
    }
}

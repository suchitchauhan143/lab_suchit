<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class functionarea extends Model
{
    public $table = "functionareas";
    protected $fillable=[
        'name',
    ];

    public function jobsadd(){
        return $this->hasMany(jobsadd::class);
    }

}

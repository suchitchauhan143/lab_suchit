<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class issue_book extends Model
{
    public $table = "issue_books";

    protected $primaryKey = 'id';
    protected $fillable=[
        'issue_date','return_date','status'
    ];

    public function book_details(){
        return $this->hasOne('App\book','id','book_id');
    }
    // public function clients(){
    //     return $this->hasMany(client::class);
    // }

    public function clients(){
        return $this->hasOne('App\client','id','client_id');
    }

}

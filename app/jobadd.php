<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jobadd extends Model
{
    protected $fillable=[ 'job_title', 'job_description', 'location', 'functioanl_area', 'job_category', 'job_time', 'work_from_home', 'vacancies', 'gender', 'minimum_age', 'maximum_age', 'qualification', 'experience', 'benefits', 'currency', 'minimun_salary', 'maximum_salary', 'organization_name', 'about_organization', 'contact', 'skill'];

    public function functionalarea(){
        return $this->hasOne('App\functionalarea','id','functional_id');
    }
    
    public function jobcategories(){
        return $this->hasOne('App\jobmake','id','category_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jobcategory extends Model
{
    public $table = "jobcategories";
    protected $fillable=[
        'name',
    ];

    public function jobsadd(){
        return $this->hasMany(jobsadd::class);
    }

}

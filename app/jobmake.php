<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jobmake extends Model
{
        public $table = "jobmakes";
        protected $fillable=['name'];

        public function jobcategories(){
            return $this->hasmany(jobmake::class);
        }

}

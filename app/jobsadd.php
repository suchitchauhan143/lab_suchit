<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jobsadd extends Model
{
    public $table = "jobsadds";

    protected $primaryKey = 'id';
    protected $fillable=[
        'job_title', 'job_description', 'location',
        'functioanl_area', 'job_category', 'job_time', 'work_from_home', 'vacancies', 'gender', 'minimum_age',
        'maximum_age', 'qualification', 'experience', 'benefits', 'currency', 'minimun_salary', 'maximum_salary',
        'organization_name', 'about_organization', 'contact', 'skill'
    ];

    public function functional_details(){
        return $this->hasOne('App\functionarea','id','functional_id');
    }
    // public function clients(){
    //     return $this->hasMany(client::class);
    // }

    public function jobcategory(){
        return $this->hasOne('App\jobcategory','id','jobcategory_id');
    }

    public function applyjobs(){
        return $this->hasMany(applyjob::class);
    }

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsaddsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobsadds', function (Blueprint $table) {
            $table->id();
            $table->string('job_title');
            $table->string('job_description');
            $table->string('location');

            $table->unsignedBigInteger('functional_id');
            $table->foreign('functional_id')->references('id')->on('functionareas');

            $table->unsignedBigInteger('jobcategory_id');
            $table->foreign('jobcategory_id')->references('id')->on('jobcategories');

            $table->string('job_time');
            $table->string('work_from_home');
            $table->string('vacancies');
            $table->string('gender');
            $table->string('minimum_age');
            $table->string('maximum_age');
            $table->string('qualification');
            $table->string('experience');
            $table->string('benefits');
            $table->string('currency');
            $table->string('minimum_salary');
            $table->string('maximum_salary');
            $table->string('organization_name');
            $table->string('about_organization');
            $table->string('contact');
            $table->string('skill');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobsadds');
    }
}

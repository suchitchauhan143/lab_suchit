@include('view')
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>todo</title>
   <style>

       </style>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href=
"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
    <div id="app">
        @include('flash')
        @yield('content')
    </div>
    <h1 style="margin-left:400px">Apply For Job</h1>
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">{{ __('Apply') }}</div>

                    <div class="card-body">

                        @if(Session::has('message'))
                            <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                        @endif

                        <form id="job_form" method="POST" action="{{ url('applyjob1',$id) }}">
                            @csrf


                            <div class="form-group">
                                <label for="full_name">{{ __('Full Name') }}</label>
                                <input name="full_name" id="full_name" placeholder="Full Name" type="text" class="form-control" required autofocus>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="number  ">{{ __('Contact Number') }}</label>
                                    <input name="number" id="number" placeholder="Contact Number" type="text" class="form-control" required autofocus>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="current_company">{{ __('Current Company') }}</label>
                                    <input name="current_company" id="current_company" placeholder="Current Company" type="text" class="form-control" required autofocus>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="current_designation">{{ __('Current Designation') }}</label>
                                    <input name="current_designation" id="current_designation" placeholder="Current Designation" type="text" class="form-control" required autofocus>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="current_location">{{ __('Current Location') }}</label>
                                    <input name="current_location" id="current_location" placeholder="Current Location" type="text" class="form-control" required autofocus>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="current_salary">{{ __('Current / Last Annual Salary') }}</label>
                                    <input name="current_salary" id="current_salary" placeholder="Current / Last Annual Salary" type="text" class="form-control" required autofocus>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="industry">{{ __('Industry') }}</label>
                                    <input name="industry" id="industry" placeholder="Industry" type="text" class="form-control" required autofocus>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="functional_area">{{ __('Functional Area') }}</label>
                                <input id="functional_area" placeholder="Functional Area" class="form-control" name="functional_area" required autofocus>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="experience_year">{{ __('Experience Year') }}</label>

                                    <select class="form-control" name="experience_year">
                                        @for($i=30; $i >= 0 ; $i--)
                                            <option value="{{$i}}" selected>{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="experience_month">{{ __('Experience Month') }}</label>

                                    <select class="form-control" name="experience_month">
                                        @for($j=12; $j >= 0 ; $j--)
                                            <option value="{{$j}}" selected>{{ $j }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="skill">{{ __('Skill') }}</label>
                                <input id="skill" placeholder="Skill" class="form-control" name="skill" autofocus>
                            </div>

                            <div class="form-group">
                                <label for="reason">{{ __('Reason For Applying') }}</label>
                                <textarea id="reason" placeholder="Reason For Applying" class="form-control" name="reason" required autofocus></textarea>
                            </div>


                            <div class="form-group">
                                <button type="submit" value="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js" integrity="sha512-37T7leoNS06R80c8Ulq7cdCDU5MNQBwlYoy1TX/WUsLFC2eYNqtKlV0QjH7r8JpG/S0GUMZwebnVFLPd6SU5yg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript">

$('#issue').validate({

rules:
{
date:"required",
book_id:"required"
},
messages:
{
date:"Please Enter date",
book_id:"Please select option"
}

});
$.validator.addMethod("digit_check",function(value) {
            return /[0-9]/.test(value);
        });

</script> --}}
</body>
</html>

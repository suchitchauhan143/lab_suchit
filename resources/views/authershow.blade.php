@include('view')
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>todo</title>
   <style>

       </style>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href=
"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
    <div class="content-wrapper">
    <div >
    <button type="button" class="btn btn-warning" ><a href="{{route('export')}}">Export</a></button><br/>
    
    <button type="button" class="btn btn-warning" ><a href="{{route('import')}}">Import</a></button><br/>
    </div>
    <table class="table table-bordered table-striped" style="margin: 1% 0 0 1%; width:95%;">
        <thead>
        <th>Auther_name</th>
        <th>Action</th>
        </thead>
        <tbody>
        @foreach($auther as $value)
        <tr>
        <td>{{ $value->name }}</td>
        <td>
            <a href="{{route('auther.delete',$value->id)}}">Delete</a><br/>
             <a href="{{route('auther.edit',$value->id)}}">Edit</a>
        </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </div>
    <div style="margin-left:500px ">
        {{ $auther->links() }}
        </div>
</html>

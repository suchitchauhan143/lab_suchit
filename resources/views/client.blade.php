@include('view')
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Client_details</title>
   <style>

       </style>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href=
"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
    <div id="app">
        @include('flash')
        @yield('content')
    </div>
    <div class="content-wrapper">
        <h1 style="margin-left: 400px">ADD Client Details</h1>
        <section class="content">
                <form id="client" action="{{ route('client/add') }}" method="post">
                    @csrf
                        Enter Client Name:<input type="text" id="name" class="form-control" placeholder="enter your name" name="name">
                        <span class="text-danger">{{ $errors->first('name') }}</span><br/>
                        Enter Client Birthdate:<input type="date" id="date" placeholder="enter date" class="form-control"
                        placeholder="todo-item" name="date">
                        <span class="text-danger">{{ $errors->first('date') }}</span><br/>
                        Enter Client Address:<input type="address" id="address" placeholder="enter address" class="form-control"
                        placeholder="todo-item" name="address">
                        <span class="text-danger">{{ $errors->first('address') }}</span><br/>
                        Enter Client Mobileno:<input type="number" id="phone" placeholder="enter mobile no." class="form-control"
                        placeholder="todo-item" name="phone">
                        <span class="text-danger">{{ $errors->first('phone') }}</span><br/>
                        Enter Client deposit:<input type="number" id="deposit" placeholder="enter deposit amount" class="form-control"
                        placeholder="todo-item" name="deposit">
                        <span class="text-danger">{{ $errors->first('deposit') }}</span><br/>
                        Enter Client Email:<input type="email" id="email" placeholder="enter your email" class="form-control"
                        placeholder="todo-item" name="email">
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                        </br>
                        <div style="margin-left: 450px">
                        <button type="submit"
                               class="btn btn-primary" id="submit">submit</button>
                        <button type="button" class="btn btn-warning" ><a href="{{url('client/show')}}">Clients Data</a></button>
                        </div>
                    </form>

            </section>
            </div>
 {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
 <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js" integrity="sha512-37T7leoNS06R80c8Ulq7cdCDU5MNQBwlYoy1TX/WUsLFC2eYNqtKlV0QjH7r8JpG/S0GUMZwebnVFLPd6SU5yg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
 <script type="text/javascript">
    $('#client').validate({
            rules:
            {
            name:"required",

            date:{
                required : true,
                date : true
            },

            address:"required",


            phone:{
            required:true,
            digit_check:true,
            maxlength:10
            },

            deposit:{
            required:true,
            digit_check:true,
            maxlength:10
            },

            email:
            {
            required : true,
            email : true
            },
            },
            messages:{
                name:"Please Enter name",

                date:{
                required:"Please enter date",
                date:"please enter date only"
                },

                address:"please enter address",

                phone:{
                required:"Please enter phone number",
                digit_check:"please enter only digit",
                maxlength:"only enter 10 digit"
                },

                deposit:{
                required:"Please enter deposit",
                digit_check:"please enter only digit",
                maxlength:"only enter 10 digit"
                },

                email:{
                    required:"please enter email",
                    email:"enter valid email"
                }


            }

            });
            $.validator.addMethod("digit_check",function(value) {
                        return /[0-9]/.test(value);
                    });

            </script>
 --}}

</body>
</html>

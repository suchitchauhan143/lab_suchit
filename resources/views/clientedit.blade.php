@include('view')
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Client_details</title>
   <style>

       </style>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href=
"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
    <div id="app">
        @include('flash')
        @yield('content')
    </div>
    <div class="content-wrapper">
        <h1>Update Client Details</h1>
        <section class="content">
                <form id="client" action="{{ route('client/edit1',$client->id) }}" method="post">
                    @csrf
                        Update Client Name:<input type="text" id="name" class="form-control" placeholder="enter your name"
                        value="{{$client->name}}" name="name"><br/>
                        Update Client Date:<input type="date" id="date" placeholder="enter date" class="form-control"
                        placeholder="todo-item"
                        value="{{$client->date_of_birth}}" name="date"><br/>
                        Update Client Address:<input type="address" id="address" placeholder="enter address" class="form-control"
                        placeholder="todo-item"
                        value="{{$client->address}}" name="address"><br/>
                        Update Client Mobileno:<input type="number" id="phone" placeholder="enter mobile no." class="form-control"
                        placeholder="todo-item"
                        value="{{$client->phone_number}}" name="phone"><br/>
                        Update Client Deposit:<input type="number" id="deposit" placeholder="enter deposit amount" class="form-control"
                        placeholder="todo-item"
                        value="{{$client->deposit}}" name="deposit"><br/>
                        Update Client Email:<input type="email" id="email" placeholder="enter your email" class="form-control"
                        placeholder="todo-item"
                        value="{{$client->email}}" name="email">
                        </br>
                        <button type="submit"
                               class="btn btn-primary" id="submit">submit</button>

                </form>
                 {{-- <a href="{{url('client/show')}}">Clients Data</a> --}}
            </section>
            </div>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
 <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js" integrity="sha512-37T7leoNS06R80c8Ulq7cdCDU5MNQBwlYoy1TX/WUsLFC2eYNqtKlV0QjH7r8JpG/S0GUMZwebnVFLPd6SU5yg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
 <script type="text/javascript">
    $('#client').validate({
            rules:
            {
            name:"required",

            date:{
                required : true,
                date : true
            },

            address:"required",


            phone:{
            required:true,
            digit_check:true,
            maxlength:10
            },

            deposit:{
            required:true,
            digit_check:true,
            maxlength:10
            },

            email:
            {
            required : true,
            email : true
            },
            },
            messages:{
                name:"Please Enter name",

                date:{
                required:"Please enter date",
                date:"please enter date only"
                },

                address:"please enter address",

                phone:{
                required:"Please enter phone number",
                digit_check:"please enter only digit",
                maxlength:"only enter 10 digit"
                },

                deposit:{
                required:"Please enter deposit",
                digit_check:"please enter only digit",
                maxlength:"only enter 10 digit"
                },

                email:{
                    required:"please enter email",
                    email:"enter valid email"
                }


            }

            });
            $.validator.addMethod("digit_check",function(value) {
                        return /[0-9]/.test(value);
                    });

            </script>


</body>
</html>

@include('view')
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>todo</title>
   <style>

       </style>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href=
"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
    <div class="content-wrapper">

    <table class="table table-bordered table-striped" style="margin: 1% 0 0 1%; width:95%;">
        <thead>
        <th>Client_name</th>
        <th>Client_birthdate</th>
        <th>Client_address</th>
        <th>Client_mobileno</th>
        <th>Client_deposit</th>
        <th>Client_email</th>
        <th>Action</th>
        <td>PDF</td>
        </thead>
        <tbody>
        @foreach($client as $value)
        <tr>
        <td>{{ $value->name }}</td>
        <td>{{ $value->date_of_birth }}</td>
        <td>{{ $value->address }}</td>
        <td>{{ $value->phone_number }}</td>
        <td>{{ $value->deposit }}</td>
        <td>{{ $value->email }}</td>
        <td>
             <a href="{{route('client.delete',$value->id)}}">Delete</a><br/>
             <a href="{{route('client.edit',$value->id)}}">Edit</a>
        </td>
        <td><button type="button" class="btn btn-warning" ><a href="{{route('client.delete',$value->id)}}" id="b1">Generate PDF</a><br/>

        </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </div>

</html>

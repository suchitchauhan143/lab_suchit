@include('view')
<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <title>todo</title>
    <style>
    </style>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
    <div id="app">
        @include('flash')
        @yield('content')
    </div>
    <div class="content-wrapper">
        <h1 style="margin-left: 400px">ADD BOOK DETAILS</h1>
        <section class="content">
            <form id="todo" action="{{ route('book/add') }}" method="post">
                @csrf
                <div>
                Enter Book Name:<input type="text" id="name" class="form-control" placeholder="Enter book name"
                    name="name">
                <span class="text-danger">{{ $errors->first('name') }}</span><br />
                </div>
                Enter Book Price:<input type="text" id="price" placeholder="enter price" class="form-control"
                    placeholder="todo-item" name="price">
                <span class="text-danger">{{ $errors->first('price') }}</span><br />
                Select Auther name:<select id="auther_name" name="auther_id" class="form-control">
                    <option value="">Select Auther</option>
                    @foreach ($auther_details as $auther_detail)
                        <option value="{{ $auther_detail->id }}">{{ $auther_detail->name }}</option>
                    @endforeach
                </select>
                <span class="text-danger">{{ $errors->first('auther_id') }}</span>
                </br>
                <div style="margin-left: 450px">
                <button type="submit" class="btn btn-primary" id="submit">submit</button>
                <button type="button" class="btn btn-warning" ><a href="{{ url('book/show') }}">Books Data</a></button>
                </div>
            </form>



        </section>
    </div>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js" integrity="sha512-37T7leoNS06R80c8Ulq7cdCDU5MNQBwlYoy1TX/WUsLFC2eYNqtKlV0QjH7r8JpG/S0GUMZwebnVFLPd6SU5yg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript">

$('#todo').validate({

rules:{
name:"required",
price:{
required:true,
digit_check:true,
maxlength:5
},
auther_id:"required"
},
messages:{
    name:"Please Enter name",
    price:{
    required:"Please enter price",
    digit_check:"please enter number",
    maxlength:"please enter only 5 digit"
    },
    auther_id:"Please select option"
}

});
$.validator.addMethod("digit_check",function(value) {
            return /[0-9]/.test(value);
        });

</script> --}}
</body>

</html>

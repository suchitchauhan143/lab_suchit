@include('view')
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Issue_book</title>
   <style>

       </style>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href=
"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" ></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"  />

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    <div id="app">
        @include('flash')
        @yield('content')
    </div>
    <div class="content-wrapper">
        <table class="table table-bordered table-striped" style="margin: 1% 0 0 1%; width:95%;">
            <thead>
            <th>issue date</th>
            <th>Return date</th>
            <th>creatd_at</th>
            <th>update_at</th>
            <th>book_name</th>
            <th>client_name</th>
            <th>Status</th>
            <th>Action</th>
            <th>PDF</th>
            </thead>
             <tbody>
            @foreach($issue as $value)
            <tr>
            <td>{{ $value->issue_date }}</td>
            <td>{{ $value->return_date }}</td>
            <td>{{ $value->created_at}}</td>
            <td>{{ $value->updated_at}}</td>
            <td>{{ $value->book_details['name']}}</td>
            <td>{{ $value->clients['name']}}</td>
            <td>
            <input data-id="{{$value->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $value->status ? 'checked' : '' }} name="status"
            @if ($value->status==0)
            disabled
            @endif></td>
            <td>
            <a href="{{route('issue.delete',$value->id)}}">Delete</a><br/>
            <a href="{{route('issue.edit',$value->id)}}">Edit</a>
            </td>
            <td>
            <button type="button" class="btn btn-warning" ><a href="{{url('pdf',$value->id)}}" id="b1">Generate PDF</a><br/>
            </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
        <script>

            $(function() {

              $('.toggle-class').change(function() {
                  var status = $(this).prop('checked') == true ? 1 : 0;
                  var book_id = $(this).data('id');
                  $.ajax({

                      type: "GET",

                      dataType: "json",

                      url: '/changeStatus',

                      data: {'status': status, 'id': book_id},

                      success: function(){
                        location.reload();
                        console.log("successfully");

                      }

                  });

              })

            })

          </script>

</body>
</html>

@include('view')
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>todo</title>
   <style>

       </style>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href=
"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
    <div class="content-wrapper">
    <table class="table table-bordered table-striped" style="margin: 1% 0 0 1%; width:95%;">
        <thead>
        <th>Title</th>
        <th>Description</th>
        <th>Action</th>
        
        <th>functional_area</th>
        <th>job_category</th>
        <th>job_time</th>
        
        <th>work_from_home</th>
        <th>vacancies</th>
        <th>gender</th>
        <th>minimum_age</th>
        <th>maximum_age</th>

        <th>qualification</th>
        <th>experience</th>
        <th>benefits</th>
        <th>currency</th>
        <th>minimum_salary</th>

        
        <th>maximum_salary</th>
        <th>organization_name</th>
        <th>contact</th>
        <th>skill</th>

        <th>Action</th>
        </thead>
        <tbody>
        @foreach($jobs as $value)
        <tr>
        <td>{{ $value->job_title }}</td>
        <td>{{ $value->job_description }}</td>
        
        <td>{{ $value->location }}</td>
        <td>{{ $value->functionalarea['name'] }}</td>
        <td>{{ $value->jobcategories['name'] }}</td>
        
        <td>{{ $value->job_time }}</td>
        
        <td>{{ $value->work_from_home }}</td>
        <td>{{ $value->vacancies }}</td>  
        <td>{{ $value->gender }}</td>
        <td>{{ $value->minimum_age }}</td>

        <td>{{ $value->maximum_age }}</td>
        <td>{{ $value->qualification }}</td>
        <td>{{ $value->experience }}</td>
        <td>{{ $value->benefits }}</td>
        
        <td>{{ $value->currency }}</td>
        <td>{{ $value->minimum_salary }}</td>
        <td>{{ $value->maximum_salary }}</td>
        <td>{{ $value->organization_name }}</td>
        
        <td>{{ $value->contact }}</td>
        <td>{{ $value->skill }}</td>
        

        <td>
            <a href="{{route('Jobs.delete',$value->id)}}">Delete</a><br/>
            <a href="{{route('Jobs.edit',$value->id)}}">Edit</a>
        </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </div>

</html>

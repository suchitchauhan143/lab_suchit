@include('view')
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>todo</title>
   <style>

       </style>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href=
"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
    <div class="content-wrapper">
    <table class="table table-bordered table-striped" style="margin: 1% 0 0 1%; width:95%;">
        <thead>
        <th>Job Name</th>
        <th>Action</th>
        </thead>
        <tbody>
        @foreach($jobs as $value)
        <tr>
        <td>{{ $value->name }}</td>
   
        <td>
            <a href="{{route('Jobs.delete',$value->id)}}">Delete</a><br/>
            <a href="{{route('Jobs.edit',$value->id)}}">Edit</a>
        </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </div>

</html>

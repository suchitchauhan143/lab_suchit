@include('view')
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>todo</title>
   <style>

       </style>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href=
"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
    <div id="app">
        @include('flash')
        @yield('content')
    </div>
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">{{ __('Create Job') }}</div>

                    <div class="card-body">

                        @if(Session::has('message'))
                            <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                        @endif

                        <form id="job_form" method="POST" action="{{ route('jobs/add1') }}">
                            @csrf


                            <div class="form-group">
                                <label for="job_title">{{ __('Job Title') }}</label>
                                <input name="job_title" id="job_title" placeholder="Job Title" type="text" class="form-control @error('job_title') is-invalid @enderror" autofocus>
                                <span class="text-danger">{{ $errors->first('job_title') }}</span>
                                <br/>
                            </div>


                            <div class="form-group">
                                <label for="job_description">{{ __('Job Description') }}</label>
                                <textarea id="job_description" placeholder="Job Description" class="form-control @error('job_description') is-invalid @enderror" name="job_description" autofocus></textarea>
                                <span class="text-danger">{{ $errors->first('job_description') }}</span>
                                <br/>
                            </div>

                            <div class="form-group">
                                <label for="location">{{ __('Location') }}</label>
                                    <select class="form-control" name="location">
                                        <option selected value="india">India</option>
                                        <option value="us">US</option>
                                        <option value="japan">Japan</option>
                                    </select>
                                    <span class="text-danger">{{ $errors->first('location') }}</span>
                                    <br/>
                            </div>

                            <div class="form-group">
                                <label for="functional_area">{{ __('Functional Area') }}</label>
                                    <select class="form-control" name="functional_area">
                                        @foreach($function as $functional)
                                            <option value="{{ $functional->id }}">{{ $functional->name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger">{{ $errors->first('functional_area') }}</span>
                                    <br/>
                            </div>

                            <div class="form-group">
                                <label for="job_category">{{ __('Job Category') }}</label>
                                    <select class="form-control" name="job_category">
                                        @foreach($jobcategory as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger">{{ $errors->first('job_category') }}</span>
                                    <br/>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="job_time">{{ __('Job Time') }}</label>

                                    <select name="job_time" class="form-control">
                                        <option value="fulltime" selected>Full Time</option>
                                        <option value="partime">Part Time</option>
                                    </select>
                                    <span class="text-danger">{{ $errors->first('job_time') }}</span>
                                    <br/>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="work_from_home">{{ __('Work From Home') }}</label>

                                    <select class="form-control" name="work_from_home">
                                        <option value="yes" selected>Yes</option>
                                        <option value="no">No</option>
                                    </select>
                                    <span class="text-danger">{{ $errors->first('work_from_home') }}</span>
                                    <br/>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="vacancies">{{ __('Number of Vacancies') }}</label>
                                    <input id="vacancies" placeholder="Vacancies" type="text" class="form-control @error('vacancies') is-invalid @enderror" name="vacancies">
                                    <span class="text-danger">{{ $errors->first('vacancies') }}</span>
                                    <br/>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="gender">{{ __('Gender Preference') }}</label>

                                    <select class="form-control" name="gender">
                                        <option value="any" selected>Any</opti  on>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                    <span class="text-danger">{{ $errors->first('gender') }}</span>
                                    <br/>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="minimum_age">{{ __('Minimum Age') }}</label>
                                    <input id="minimum_age" placeholder="Minimum Age" type="text" class="form-control @error('minimum_age') is-invalid @enderror" name="minimum_age">
                                    <span class="text-danger">{{ $errors->first('minimum_age') }}</span>
                                    <br/>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="maximum_age">{{ __('Maximum Age') }}</label>
                                    <input id="maximum_age" placeholder="Maximum Age" type="text" class="form-control @error('maximum_age') is-invalid @enderror" name="maximum_age">
                                    <span class="text-danger">{{ $errors->first('maximum_age') }}</span>
                                    <br/>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="qualificaion">{{ __('Qualificaion') }}</label>
                                    <input id="qualification" placeholder="Qualification" type="text" class="form-control @error('qualification') is-invalid @enderror" name="qualification">
                                    <span class="text-danger">{{ $errors->first('qualification') }}</span>
                                    <br/>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="experience">{{ __('Experience') }}</label>
                                    <input id="experience" placeholder="Experience" type="text" class="form-control @error('experience') is-invalid @enderror" name="experience">
                                    <span class="text-danger">{{ $errors->first('experience') }}</span>
                                    <br/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="benefits">{{ __('Job Benefits') }}</label>
                                <textarea id="benefits" placeholder="Job Benefits" class="form-control @error('benefits') is-invalid @enderror" name="benefits" autofocus></textarea>
                                <span class="text-danger">{{ $errors->first('benefits') }}</span>
                                    <br/>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="currency">{{ __('Salary Currency') }}</label>

                                    <select class="form-control" name="currency">
                                        <option value="rupees" selected>Rupees</option>
                                        <option value="dolar">Dolar</option>
                                    </select>
                                    <span class="text-danger">{{ $errors->first('currency') }}</span>
                                    <br/>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="minimum_salary">{{ __('Minimum Salary') }}</label>
                                    <input id="minimum_salary" placeholder="Minimum Salary" type="text" class="form-control @error('minimum_salary') is-invalid @enderror" name="minimum_salary" >
                                    <span class="text-danger">{{ $errors->first('minimum_salary') }}</span>
                                    <br/>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="maximum_salary">{{ __('Maximum Salary') }}</label>
                                    <input id="maximum_salary" placeholder="Maximum Salary" type="text" class="form-control @error('maximum_salary') is-invalid @enderror" name="maximum_salary" >
                                    <span class="text-danger">{{ $errors->first('maximum_salary') }}</span>
                                    <br/>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="organization_name">{{ __('Organization Name') }}</label>
                                <input id="organization_name" placeholder="Organization Name" type="text" class="form-control @error('organization_name') is-invalid @enderror" name="organization_name" autofocus>
                                <span class="text-danger">{{ $errors->first('organization_name') }}</span>
                                    <br/>
                            </div>

                            <div class="form-group">
                                <label for="about_organization">{{ __('About Organization') }}</label>
                                <textarea id="about_organization" placeholder="About Organization" class="form-control @error('about_organization') is-invalid @enderror" name="about_organization" autofocus></textarea>
                                <span class="text-danger">{{ $errors->first('about_organization') }}</span>
                                    <br/>
                            </div>

                            <div class="form-group">
                                <label for="contact">{{ __('Contact Details') }}</label>
                                <input id="contact" placeholder="Contact details" type="text" class="form-control @error('contact') is-invalid @enderror" name="contact">
                                <span class="text-danger">{{ $errors->first('contact') }}</span>
                                    <br/>
                            </div>

                            <div class="form-group">
                                <label for="skill">{{ __('Required skill') }}</label>
                                <input id="skill" placeholder="Required skill" type="text" class="form-control @error('skill') is-invalid @enderror" name="skill">
                                <span class="text-danger">{{ $errors->first('skill') }}</span>
                                    <br/>
                            </div>


                            <div class="form-group">
                                    <button type="submit" value="submit" class="btn btn-primary">
                                        {{ __('Submit') }}
                                    </button>
                   <button type="button" class="btn btn-warning" ><a href="{{url('jobs/show')}}">Jobs Data</a></button>

                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

        {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js" integrity="sha512-37T7leoNS06R80c8Ulq7cdCDU5MNQBwlYoy1TX/WUsLFC2eYNqtKlV0QjH7r8JpG/S0GUMZwebnVFLPd6SU5yg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script type="text/javascript">

        $('#auther').validate({

        rules:{
        auther_name:"required"
        },
        messages:{
        auther_name:"Please Enter name"
        }

        });

        </script> --}}
</body>
</html>

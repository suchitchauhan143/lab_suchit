
@include('view')
<div class="container">
    <div class="row justify-content-end">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">{{ __('Create Job') }}</div>

                <div class="card-body">

                    @if(Session::has('message'))
                        <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                    @endif

                    <form id="job_form" method="POST" action="{{ route('jobsee/add') }}">
                        @csrf
                        

                        <div class="form-group">
                            <label for="job_title">{{ __('Job Title') }}</label>
                            <input name="job_title" id="job_title" placeholder="Job Title" type="text" class="form-control @error('job_title') is-invalid @enderror" autofocus>
                        </div>

                        
                        <div class="form-group">
                            <label for="job_description">{{ __('Job Description') }}</label>
                            <textarea id="job_description" placeholder="Job Description" class="form-control @error('job_description') is-invalid @enderror" name="job_description" autofocus></textarea>
                        </div>
                        
                        <div class="form-group">
                            <label for="location">{{ __('Location') }}</label>                
                                <select class="form-select" name="location">
                                    <option selected value="india">India</option>
                                    <option value="us">US</option>
                                    <option value="japan">Japan</option>
                                </select>
                        </div>
        
                        <div class="form-group">
                            <label for="functional_area">{{ __('Functional Area') }}</label>
                                <select class="form-select" name="functional_area"> 
                                    @foreach($functional as $functionala)
                                        <option value="{{ $functionala->id }}">{{ $functionala->name }}</option>
                                    @endforeach
                                </select>
                        </div>

                        <div class="form-group">
                            <label for="job_category">{{ __('Job Category') }}</label>
                                <select class="form-select" name="job_category">
                                    @foreach($jobs as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>     
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="job_time">{{ __('Job Time') }}</label>                                

                                <select name="job_time" class="form-select">
                                    <option value="fulltime" selected>Full Time</option>
                                    <option value="partime">Part Time</option>
                                </select>     
                            </div>

                            <div class="form-group col-md-6">
                                <label for="work_from_home">{{ __('Work From Home') }}</label>
                                
                                <select class="form-select" name="work_from_home">
                                    <option value="yes" selected>Yes</option>
                                    <option value="no">No</option>
                                </select>     
                            </div>
                        </div>    

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="vacancies">{{ __('Number of Vacancies') }}</label>                                
                                <input id="vacancies" placeholder="Vacancies" type="text" class="form-control @error('vacancies') is-invalid @enderror" name="vacancies" required>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="gender">{{ __('Gender Preference') }}</label>
                                
                                <select class="form-select" name="gender">
                                    <option value="any" selected>Any</opti  on>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>     
                            </div>
                        </div>    


                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="minimum_age">{{ __('Minimum Age') }}</label>
                                <input id="minimum_age" placeholder="Minimum Age" type="text" class="form-control @error('minimum_age') is-invalid @enderror" name="minimum_age" required>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="maximum_age">{{ __('Maximum Age') }}</label>
                                <input id="maximum_age" placeholder="Maximum Age" type="text" class="form-control @error('maximum_age') is-invalid @enderror" name="maximum_age" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="qualificaion">{{ __('Qualificaion') }}</label>
                                <input id="qualification" placeholder="Qualification" type="text" class="form-control @error('qualification') is-invalid @enderror" name="qualification" required>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="experience">{{ __('Experience') }}</label>                                
                                <input id="experience" placeholder="Experience" type="text" class="form-control @error('experience') is-invalid @enderror" name="experience" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="benefits">{{ __('Job Benefits') }}</label>
                            <textarea id="benefits" placeholder="Job Benefits" class="form-control @error('benefits') is-invalid @enderror" name="benefits" required autofocus></textarea>
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="currency">{{ __('Salary Currency') }}</label>
                                
                                <select class="form-select" name="currency">
                                    <option value="rupees" selected>Rupees</option>
                                    <option value="dolar">Dolar</option>
                                </select>     
                            </div>

                            <div class="form-group col-md-4">
                                <label for="minimum_salary">{{ __('Minimum Salary') }}</label>
                                <input id="minimum_salary" placeholder="Minimum Salary" type="text" class="form-control @error('minimum_salary') is-invalid @enderror" name="minimum_salary" required>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="maximum_salary">{{ __('Maximum Salary') }}</label>                                
                                <input id="maximum_salary" placeholder="Maximum Salary" type="text" class="form-control @error('maximum_salary') is-invalid @enderror" name="maximum_salary" required>
                            </div>
                        </div>    


                        <div class="form-group">
                            <label for="organization_name">{{ __('Organization Name') }}</label>
                            <input id="organization_name" placeholder="Organization Name" type="text" class="form-control @error('organization_name') is-invalid @enderror" name="organization_name" required autofocus>
                        </div>

                        <div class="form-group">
                            <label for="about_organization">{{ __('About Organization') }}</label>
                            <textarea id="about_organization" placeholder="About Organization" class="form-control @error('about_organization') is-invalid @enderror" name="about_organization" required autofocus></textarea>
                        </div>

                        <div class="form-group">
                            <label for="contact">{{ __('Contact Details') }}</label>
                            <input id="contact" placeholder="Contact details" type="text" class="form-control @error('contact') is-invalid @enderror" name="contact" required>        
                        </div>

                        <div class="form-group">
                            <label for="skill">{{ __('Required skill') }}</label>                        
                            <input id="skill" placeholder="Required skill" type="text" class="form-control @error('skill') is-invalid @enderror" name="skill" required>
                        </div>


                        <div class="form-group">
                                <button type="submit" value="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                        </div>


                    </form>
                                             <a href="{{url('Jobsee/show')}}">Jobs Data</a>

                </div>
            </div>
        </div>
    </div>
</div>

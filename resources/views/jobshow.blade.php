@include('view')
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>todo</title>
   <style>

       </style>

    <!-- Latest compiled and minified CSS -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body>
    <br/>
    <div class="content-wrapper">
    <table class="data-table table table-striped table-bordered" style="width: 95%">
        <thead>
            <tr>
        <th>job_title</th>
        <th>job_description</th>
        <th>Action</th>
        <th>Apply_Job</th>
        <th>Paticipantes</th>
            </tr>
    </thead>
        <tbody>
        </tbody>
    </table>
    </div>
    <script type="text/javascript">
       $(function () {

    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('users.index') }}",
        columns: [
            {data: 'job_title', name: 'job_title'},
            {data: 'job_description', name: 'job_description'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
            {data: 'Apply_Job', name: 'Apply_Job', orderable: false,},
            {data: 'Paticipantes', name: 'Paticipantes', orderable: false,},
        ]
    });
  });



    </script>
</body>
</html>

<!DOCTYPE html>
<html>
<head>
	<title>User PDF</title>
<style>  


      th {
      background-color: rgba(38,137,13,1);
      border: 1px solid rgba(134,188,37,1);
      font-weight: normal;
      text-align: center;
      color: white;
      
      }
      td
      {
        background-color:#6A679E;
        border-left: 1px solid rgba(134,188,37,1);
        border-bottom: 1px solid rgba(134,188,37,1);
        text-align: center; 
        font-size: 20px;
      }



</style> 
</head>
<body>

   
  <header style="position: fixed;top:-19px; left:145px">
            <div>
                <img src="{{ $data['image'] }}" style="width: 20px; height: 20px">
            </div>
    </header>
        <table>
            <thead>
            <th>Name</th>
            <th>Address</th>
            <th>Date OF Birth</th>
            <th>Email</th>
            <th>Deposit</th>
            <th>Phono Number</th>
            <th>creatd_at</th>
            <th>update_at</th>
           
            </thead>
             <tbody>
            @foreach($data['user'] as $value)
            <tr>
            <td>{{ $value['name'] }}</td>
            <td>{{ $value['address'] }}</td>
            <td>{{ $value['date_of_birth'] }}</td>
            <td>{{ $value['email'] }}</td>
            <td>{{ $value['deposit'] }}</td>
            <td>{{ $value['phone_number'] }}</td>
            <td>{{ $value['created_at']}}</td>
            <td>{{ $value['updated_at']}}</td>
           
            @endforeach
            
            <script type="text/php">
    if ( isset($pdf) ) {
        $x1 =72;
        $y1 = 18;
        $text1 = "Client Details";
         $font1 = $fontMetrics->get_font("helvetica", "bold");
        $size1 = 12;
        $color1 = array(255,0,0);
        $word_space1 = 0.0;  //  default
        $char_space1 = 0.0;  //  default
        $angle1 = 0.0;   //  default
        $pdf->page_text($x1, $y1, $text1, $font1, $size1, $color1, $word_space1, $char_space1, $angle1);
        
        
        $x =390;
        $y = 575;
        $text = "{PAGE_NUM} of {PAGE_COUNT}";
        $font = $fontMetrics->get_font("helvetica", "bold");
        $size = 12;
        $color = array(255,0,0);
        $word_space = 0.0;  //  default
        $char_space = 0.0;  //  default
        $angle = 0.0;   //  default
        $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
    }
</script>
</body>
</html>

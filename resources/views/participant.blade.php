@include('view')
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>todo</title>
   <style>

       </style>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
{{-- <script type = "text/javascript" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css" /> --}}
</head>
<body>
    <div class="content-wrapper">
    <table class="table table-bordered table-striped" style="margin: 1% 0 0 1%; width:95%;" id="job">
        <thead>
        <tr>
        <th>Fullname</th>
        <th>Contact No.</th>
        </tr>
    </thead>
        <tbody>
            @foreach($participant as $value)
            <tr>
            <td>{{ $value->full_name }}</td>
            <td>{{ $value->number}}</td>
           </tr>
            @endforeach
        </tbody>
    </table>
    </div>
    {{-- <script type="text/javascript">
       $(function () {

    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('users.index') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

  });
    </script> --}}
</body>
</html>

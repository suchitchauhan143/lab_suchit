<!DOCTYPE html>
<html>
<head>
    <title>Login Form</title>
    <style>
        body
{
    margin: 0;
    padding: 0;
    background-color:#6abadeba;
    font-family: 'Arial';
}
.login{
        width: 382px;
        overflow: hidden;
        margin: auto;
        margin: 20 0 0 450px;
        padding: 80px;
        background: #23463f;
        border-radius: 15px ;

}
h2{
    text-align: center;
    color: #277582;
    padding: 20px;
}
label{
    color: #08ffd1;
    font-size: 17px;
}
#Uname{
    width: 300px;
    height: 30px;
    border: none;
    border-radius: 3px;
    padding-left: 8px;
}
#Pass{
    width: 300px;
    height: 30px;
    border: none;
    border-radius: 3px;
    padding-left: 8px;

}
#log{
    width: 300px;
    height: 30px;
    border: none;
    border-radius: 17px;
    padding-left: 7px;
    color: blue;


}
span{
    color: white;
    font-size: 17px;
}
a{
    float: right;
    background-color: grey;
}
        </style>
</head>
<body>
    <h2>Register Page</h2><br>
    <div class="login">
    <form id="register" action="{{ route('register1') }}" method="post">
        @csrf
        <label><b>Name:
        </b>
        </label>
        <input type="text" name="name" id="name" placeholder="Username">
        <br><br>
        <label><b>Email:
        </b>
        </label>
        <input type="email" name="email" id="email" placeholder="Username">
        <br><br>
        <label><b>Password
        </b>
        </label>
        <input type="Password" name="password" id="password" placeholder="Password">
        <br><br>
        <button type="submit" name="log" class="btn btn-success" id="log">Register</button>
        <br><br>
    </form>
</div>
</body>
</html>

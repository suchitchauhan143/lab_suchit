<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\JWTController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
 
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('function','api\functioncontroller@index')->middleware('auth:api');
Route::post('functioncreate','api\functioncontroller@store')->middleware('auth:api');
Route::delete('functiondelete/{id}','api\functioncontroller@destroy')->middleware('auth:api');
Route::put('functionupdate/{id}','api\functioncontroller@update')->middleware('auth:api');

Route::get('function1','api\jobcategries@index')->middleware('auth:api');
Route::post('functioncreate1','api\jobcategries@store')->middleware('auth:api');
Route::delete('functiondelete1/{id}','api\jobcategries@destroy')->middleware('auth:api');
Route::put('functionupdate1/{id}','api\jobcategries@update')->middleware('auth:api');


Route::get('function2','api\jobapply@index')->middleware('auth:api');
Route::post('functioncreate2','api\jobapply@store')->middleware('auth:api');
Route::delete('functiondelete2/{id}','api\jobapply@destroy')->middleware('auth:api');
Route::put('functionupdate2/{id}','api\jobapply@update')->middleware('auth:api');


Route::get('function3','api\users@index')->middleware('auth:api');
Route::post('functioncreate3','api\users@store')->middleware('auth:api');
Route::delete('functiondelete3/{id}','api\users@destroy')->middleware('auth:api');
Route::put('functionupdate3/{id}','api\users@update')->middleware('auth:api');


Route::get('function4','api\clientapi@index')->middleware('auth:api');
Route::post('functioncreate4','api\clientapi@store')->middleware('auth:api');
Route::delete('functiondelete4/{id}','api\clientapi@destroy')->middleware('auth:api');
Route::put('functionupdate4/{id}','api\clientapi@update')->middleware('auth:api');


Route::get('function5','api\applyjobapi@index')->middleware('auth:api');
Route::post('functioncreate5','api\applyjobapi@store')->middleware('auth:api');
Route::delete('functiondelete5/{id}','api\applyjobapi@destroy')->middleware('auth:api');
Route::put('functionupdate5/{id}','api\applyjobapi@update')->middleware('auth:api');




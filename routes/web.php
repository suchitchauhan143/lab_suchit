<?php

use App\functionarea;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
// use Illuminate\Support\Facades\Mail;
// use App\Mail\SendEmailTest;
use App\Http\Controllers\BookController;
use App\Http\Controllers\AddController;
use App\Http\Controllers\ApplyjobController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\IssueBookController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\FacebookController;
use App\Http\Controllers\FunctionareaController;
use App\Http\Controllers\JobcategoryController;
use App\Http\Controllers\JobsaddController;

use App\jobsadd;


// use Mail;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

// //login to admin panel
 Route::post('login', [UserController::class, 'LogIn'])->name('login')->middleware('authcheck');
Route::get('register', [UserController::class, 'register']);
Route::post('register1', [UserController::class, 'register1'])->name('register1');
//routes of all book details
Route::post('book/add', [BookController::class, 'add'])->name('book/add')->middleware('authcheck');
Route::get('book/show', [BookController::class, 'show'])->name('book/show')->middleware('authcheck');
Route::get('book/delete/{id}', [BookController::class, 'delete'])->name('book.delete')->middleware('authcheck');
Route::get('book/edit/{id}', [BookController::class, 'edit'])->name('book.edit')->middleware('authcheck');
Route::post('book/edit1/{id}', [BookController::class, 'edit1'])->name('book/edit1')->middleware('authcheck');
Route::get('bookpdf',[BookController::class, 'pdf']);
//routes of all auther details
Route::post('auther/add1', [AddController::class, 'add1'])->name('auther/add1')->middleware('authcheck');
Route::get('auther/show', [AddController::class, 'show'])->name('auther/show')->middleware('authcheck');
Route::get('auther/delete/{id}', [AddController::class, 'delete'])->name('auther.delete')->middleware('authcheck');
Route::get('auther/edit/{id}', [AddController::class, 'edit'])->name('auther.edit')->middleware('authcheck');
Route::post('auther/edit1/{id}', [AddController::class, 'edit1'])->name('auther/edit1')->middleware('authcheck');
//routes of all functionarea details
Route::post('function/add1', [FunctionareaController::class, 'add1'])->name('function/add1')->middleware('authcheck');
Route::get('function/show', [FunctionareaController::class, 'show'])->name('function/show')->middleware('authcheck');
Route::get('function/delete/{id}', [FunctionareaController::class, 'delete'])->name('function.delete')->middleware('authcheck');
Route::get('function/edit/{id}', [FunctionareaController::class, 'edit'])->name('function.edit')->middleware('authcheck');
Route::post('function/edit1/{id}', [FunctionareaController::class, 'edit1'])->name('function/edit1')->middleware('authcheck');
//routes of all jobcategory details
Route::post('jobcategory/add1', [JobcategoryController::class, 'add1'])->name('jobcategory/add1')->middleware('authcheck');
Route::get('jobcategory/show', [JobcategoryController::class, 'show'])->name('jobcategory/show')->middleware('authcheck');
Route::get('jobcategory/delete/{id}', [JobcategoryController::class, 'delete'])->name('jobcategory.delete')->middleware('authcheck');
Route::get('jobcategory/edit/{id}', [JobcategoryController::class, 'edit'])->name('jobcategory.edit')->middleware('authcheck');
Route::post('jobcategory/edit1/{id}', [JobcategoryController::class, 'edit1'])->name('jobcategory/edit1')->middleware('authcheck');
//
Route::post('jobs/add1', [JobsaddController::class, 'add1'])->name('jobs/add1')->middleware('authcheck');
Route::get('jobs/show', [JobsaddController::class, 'show'])->name('jobs/show')->middleware('authcheck');
Route::get('/deletejob/{id}', 'JobsaddController@delete')->middleware('authcheck');
Route::get('/updatejob/{id}', 'JobsaddController@edit')->middleware('authcheck');
Route::get('/applyjob/{id}', 'JobsaddController@apply')->middleware('authcheck');
Route::get('/participants/{id}', 'JobsaddController@participant')->middleware('authcheck');
Route::post('jobs/edit1/{id}', [JobsaddController::class, 'edit1'])->name('jobs/edit1')->middleware('authcheck');
// Routes for apply jobs
Route::post('applyjob1/{id}', [ApplyjobController::class, 'index'])->name('applyjob1')->middleware('authcheck');
Route::get('users', ['uses'=>'JobsaddController@index', 'as'=>'users.index']);
//routes of all issue_book details
Route::post('issue/add', [IssueBookController::class, 'add'])->name('issue/add')->middleware('authcheck');
Route::get('issue/show', [IssueBookController::class, 'show'])->name('issue.show')->middleware('authcheck');
Route::get('issue/delete/{id}', [IssueBookController::class, 'delete'])->name('issue.delete')->middleware('authcheck');
Route::get('issue/edit/{id}', [IssueBookController::class, 'edit'])->name('issue.edit')->middleware('authcheck');
Route::post('issue/edit1/{id}', [IssueBookController::class, 'edit1'])->name('issue/edit1')->middleware('authcheck');
Route::get('changeStatus',[IssueBookController::class, 'status']);
Route::get('pdf/{id}',[IssueBookController::class, 'pdf']);
//routes of all client details
Route::post('client/add', [ClientController::class, 'add'])->name('client/add')->middleware('authcheck');
Route::get('client/show', [ClientController::class, 'show'])->name('client/show')->middleware('authcheck');
Route::get('client/delete/{id}', [ClientController::class, 'delete'])->name('client.delete')->middleware('authcheck');
Route::get('client/edit/{id}', [ClientController::class, 'edit'])->name('client.edit')->middleware('authcheck');
Route::post('client/edit1/{id}', [ClientController::class, 'edit1'])->name('client/edit1')->middleware('authcheck');
//routes of sidebar to book
Route::get('home', [UserController::class, 'index'])->name('home')->middleware('authcheck');
//routes of sidebar to auther
Route::get('auther', [UserController::class, 'index1'])->name('auther')->middleware('authcheck');
//routes of sidebar to issue_book
Route::get('issue', [UserController::class, 'index2'])->name('issue')->middleware('authcheck');
//route of sidebar to client
Route::get('client', [UserController::class, 'index3'])->name('client')->middleware('authcheck');
//route of sidebar to BookType
Route::get('booktype', [UserController::class, 'index4'])->name('booktype')->middleware('authcheck');
//routes of sidebar to job category
Route::get('function', [UserController::class, 'index7'])->name('function')->middleware('authcheck');
//route of sidebar to functionarea
Route::get('jobcategory', [UserController::class, 'index8'])->name('jobcategory')->middleware('authcheck');
//
Route::get('jobs', [UserController::class, 'index9'])->name('jobs')->middleware('authcheck');
//logina and logout
Route::get('logout', [UserController::class, 'index5'])->name('logout');
Route::get('authlogin', [UserController::class, 'index6'])->name('authlogin');
Auth::routes();

Route::get('/library', 'HomeController@index')->name('library');
Route::get('auth/google', 'GoogleController@redirectToGoogle');
Route::get('auth/google/callback', 'GoogleController@handleGoogleCallback');
Route::get('auth/facebook', [FacebookController::class, 'redirectToFacebook']);
Route::get('auth/facebook/callback', [FacebookController::class, 'handleFacebookCallback']);

Route::get('export', 'AddController@export')->name('export');
Route::post('import', 'AddController@import')->name('import');
Route::get('importExportView', 'AddController@importExportView');

Route::get('import', 'AddController@importExportView');
Route::get('export', 'AddController@export')->name('export');
Route::post('import', 'AddController@import')->name('import');